# Get Started

## You can view a live implementation of the project here:

[CloudFront Link](d2n8w035lczvcs.cloudfront.net)

## To run the project locally:

- Clone the project into a local repository
- Run `yarn` in the project directory
- Run `yarn start` for the app to run

## To deploy the project locally:

- Define your AWS profile in aws .config and .credentials files
- Define a system parameter in your OS named `AWS_PROFILE` and set its value the same as your AWS Profile name
- Run  `yarn deploy:backend`  from the project directory


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn deploy:backend`

Deploys the serverless using deploy script in the serverless directory



## Available Scripts in the serverless directory:

In the project directory, you can run:


### `npm run test`

Launches the tests

### `npm run deploy`

runs sls deploy using default parameters that is set




