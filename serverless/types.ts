export interface Company {
  _id: string
  name: string
  logoUrl?: string
  specialties: string[]
  city?: string
}
