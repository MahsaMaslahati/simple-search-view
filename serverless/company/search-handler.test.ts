import { APIGatewayEvent, APIGatewayProxyResult, Context } from "aws-lambda"
import { companies } from "./data/companies"
import { handler } from "./search-handler"
import _ from "lodash"

describe("search-handler test", () => {
  let consoleInfoSpy: jest.SpyInstance
  let consoleErrorSpy: jest.SpyInstance

  let mockContext = { callbackWaitsForEmptyEventLoop: false } as Context
  let mockEvent = {
    resource: `/testresource`,
    path: `/testresource`,
    httpMethod: "GET",
    headers: {
      header1: "header1value",
    },
    multiValueHeaders: {
      header1: ["header1value"],
    },
    queryStringParameters: {},
    stageVariables: null,
    requestContext: {},

    body: null,
    isBase64Encoded: false,
  } as unknown as APIGatewayEvent

  beforeEach(() => {
    consoleInfoSpy = jest.spyOn(console, "info").mockReturnValue()
    consoleErrorSpy = jest.spyOn(console, "error").mockReturnValue()
  })

  afterEach(() => {
    consoleInfoSpy.mockRestore()
    consoleErrorSpy.mockRestore()
  })

  it("Must exist as a function", () => {
    expect(handler).toBeDefined()
    expect(typeof handler).toEqual("function")
  })

  it("Must filter companies by searchPhrase if it exists in querystringParameters and return the result", async () => {
    const event = { ...mockEvent, queryStringParameters: { searchPhrase: "Cent" } } as unknown as APIGatewayEvent
    const filteredCompanies = companies.filter((company) => company.name.toLowerCase().includes("cent"))
    await expect(handler(event, mockContext)).resolves.toEqual({
      statusCode: 200,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      isBase64Encoded: false,
      body: JSON.stringify({ Items: filteredCompanies, event, context: mockContext }, null, 2),
    })
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0)
    expect(consoleErrorSpy).toHaveBeenCalledTimes(0)
  })

  it("Must filter companies by specialties if it exists in querystringParameters and return the result", async () => {
    const event = {
      ...mockEvent,
      queryStringParameters: { specialties: "Electrical,Excavation" },
    } as unknown as APIGatewayEvent
    const filteredCompanies = companies.filter(
      (company) => company.specialties.includes("Electrical") || company.specialties.includes("Excavation"),
    )

    await expect(handler(event, mockContext)).resolves.toEqual({
      statusCode: 200,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      isBase64Encoded: false,
      body: JSON.stringify({ Items: filteredCompanies, event, context: mockContext }, null, 2),
    })
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0)
    expect(consoleErrorSpy).toHaveBeenCalledTimes(0)
  })

  it("Must return all companies if no querystringParameters exists", async () => {
    await expect(handler(mockEvent, mockContext)).resolves.toEqual({
      statusCode: 200,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      isBase64Encoded: false,
      body: JSON.stringify({ Items: companies, event: mockEvent, context: mockContext }, null, 2),
    })
    expect(consoleInfoSpy).toHaveBeenCalledTimes(0)
    expect(consoleErrorSpy).toHaveBeenCalledTimes(0)
  })

  it("Must info error and return correct result if any error happened", async () => {
    const response = await handler(undefined, mockContext)
    expect(consoleInfoSpy).toHaveBeenCalledTimes(1)
    expect(consoleErrorSpy).toHaveBeenCalledTimes(1)
    expect(response).toEqual({
      statusCode: 500,
      body: JSON.stringify({ error: {} }, null, 2),
    })
  })
})
