import { APIGatewayEvent, APIGatewayProxyResult, Context } from "aws-lambda"
import { companies } from "./data/companies"
import _ from "lodash"

export const handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult | undefined> => {
  try {
    let filteredCompanies = [...companies]
    const { queryStringParameters } = event
    if (queryStringParameters?.searchPhrase) {
      filteredCompanies = filteredCompanies.filter((company) =>
        company.name.toLowerCase().includes(queryStringParameters?.searchPhrase?.toLowerCase() || ""),
      )
    }
    if (queryStringParameters?.specialties) {
      const specialtiesFilterArray = queryStringParameters?.specialties.split(",")
      filteredCompanies = filteredCompanies.filter(
        (company) => _.intersection(company.specialties, specialtiesFilterArray).length,
      )
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      isBase64Encoded: false,
      body: JSON.stringify({ Items: filteredCompanies, event, context }, null, 2),
    }
  } catch (error) {
    console.info(`Error happened on search handler, ${JSON.stringify(event)}`)
    console.error(error)
    return {
      statusCode: 500,
      body: JSON.stringify({ error: error as Error }, null, 2),
    }
  }
}
