import {
  CssBaseline,
  makeStyles,
  MuiThemeProvider,
  unstable_createMuiStrictModeTheme as createMuiTheme,
} from "@material-ui/core"
import { ThemeOptions } from "@material-ui/core/styles"
import { TypographyOptions } from "@material-ui/core/styles/createTypography"
import * as React from "react"
import { setPalette } from "./options/palette"

interface setTheme {
  typography: TypographyOptions
  palette: ThemeOptions["palette"]
}
const setTheme = (overrides?: setTheme) => {
  const palette = setPalette(overrides?.palette)

  return createMuiTheme({
    palette,
  })
}

const useStyles = makeStyles({
  "@global": {
    "html,body": {
      height: `100%`,
      width: `100%`,
      overscrollBehavior: `none`,
    },
  },
})

interface GlobalStyles {
  children: React.ReactNode
}

export const GlobalStyles = React.memo((props: GlobalStyles) => {
  const theme = setTheme()

  useStyles(theme)

  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />

      {props.children}
    </MuiThemeProvider>
  )
})

GlobalStyles.displayName = "GlobalStyles"
