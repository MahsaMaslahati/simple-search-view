import { ThemeOptions } from "@material-ui/core/styles"
import { PaletteOptions } from "@material-ui/core/styles/createPalette"
import { merge } from "lodash"

const white = `#fff`

export type PaletteColorOptions = SimplePaletteColorOptions

export interface SimplePaletteColorOptions {
  light?: string
  main: string
  dark?: string
  contrastText?: string
}

interface DefaultPaletteOptions extends PaletteOptions {
  primary?: SimplePaletteColorOptions
  secondary?: SimplePaletteColorOptions
}

export const setPalette = (override: ThemeOptions["palette"]): DefaultPaletteOptions =>
  /**
   * deep merge of defaults with the override
   */
  merge(
    /**
     * light & dark colors are generated
     * @see https://material.io/resources/color
     *
     * Default values
     */
    {
      text: {
        primary: `#242528`,
      },
      background: {
        paper: white,
        default: white,
      },
      primary: {
        light: `#7CADE8`,
        main: `#508EF2`,
        dark: `#2668AA`,
        contrastText: "#fff",
      },
      secondary: {
        light: `#E5A36C`,
        main: `#EA913A`,
        dark: `#A85720`,
        contrastText: "#fff",
      },
      error: {
        main: `#DB0021`,
      },
      success: {
        main: `#038238`,
      },
      info: {
        main: `#FC6868`,
      },
      grey: {
        100: `#96989C`,
        200: `#D3D6DC`,
        300: `#ECECEC`,
        400: `#F8F8F9`,
        A100: `#525252`,
      },
    },
    override,
  )
