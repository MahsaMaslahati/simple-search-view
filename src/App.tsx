import * as React from "react"
import { Route } from "wouter"
import { Companies } from "ssv/src/components/companies"
import { GlobalStyles } from "ssv/src/theme"

const App = (): React.ReactElement => {
  return (
    <GlobalStyles>
      <Route path={"/"} component={Companies} />
    </GlobalStyles>
  )
}

export default App
