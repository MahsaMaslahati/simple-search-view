import { responseInterface, useSwr } from "ssv/src/plugins"

export interface GetParams {
  apiUrl?: string
  path?: string
  queryStringParameters?: any
}
export const useGet = (params: GetParams): responseInterface<{ Items: any[] }, Error> => {
  const url = `${params.apiUrl}${params.path ? `/${params.path}` : ""}${
    params.queryStringParameters && Object.keys(params.queryStringParameters).length
      ? `?${new URLSearchParams(params.queryStringParameters).toString()}`
      : ""
  }`
  return useSwr<useSwr<any>>(url)
}
