import * as React from "react"
import { Box, LinearProgress, Avatar, Chip, TextField, FormControl, FormLabel, FormGroup, FormControlLabel, Checkbox } from "@material-ui/core"
import { makeStyles, Theme } from "@material-ui/core/styles"
import { VirtualizerTable, Column } from "ssv/src/components/virtualizer/table"
import { Company } from "ssv/serverless/types"
import { useGet } from "ssv/src/services/general-service"
import { Specialties } from "ssv/src/constants/specialties"

const useStyles = makeStyles((theme: Theme) => ({
    table: {
        minHeight: 700,
    },
    formControl: {
        margin: theme.spacing(3),
    },
    textField: {
        minWidth: 300,
        marginRight: theme.spacing(2)
    }
}))

export interface ColumnType {
    label: string
    accessor: string
    render?: Column["render"]
}

export const Companies: React.FC<any> = (props: any) => {
    const classes = useStyles()
    const [searchPhrase, setSearchPhrase] = React.useState<string>("")
    const [specialties, setSpecialties] = React.useState<{ [key: string]: boolean }>(Specialties.reduce((o, key) => ({ ...o, [key]: false }), {}))
    const [specialtiesQueryStringValue, setSpecialtiesQueryStringValue] = React.useState<string>()

    React.useEffect(() => {
        setSpecialtiesQueryStringValue(Specialties.filter(specialty => specialties[specialty]).join(","))
    }, [specialties])

    const { data, isValidating } = useGet({
        apiUrl: process.env.REACT_APP_COMPANY_API_URL,
        path: "company",
        queryStringParameters: Object.assign(searchPhrase ? { searchPhrase } : {}, specialtiesQueryStringValue ? { specialties: specialtiesQueryStringValue } : {})
    })
    const { Items } = data || {}

    const columns: ColumnType[] = [
        {
            label: `Logo`,
            accessor: `logoUrl`,
            render: (row: Company) => {
                return <Avatar src={row.logoUrl || "/broken-image.jpg"} />
            }
        },
        {
            label: `Company Name`,
            accessor: `name`,
        },
        {
            label: `City`,
            accessor: `city`,
        },
        {
            label: `Specialties`,
            accessor: `specialties`,
            render: (row: Company) => {
                return row.specialties?.map((specialty, index) =>
                    <Chip key={index} size="small" label={specialty} color="secondary" variant="outlined" />)
            }
        }
    ]

    const handleSearchPhraseChange = (event: any) => {
        setSearchPhrase(event?.target?.value)
    }

    const handleSpecialtiesChange = (event: any) => {
        setSpecialties({ ...specialties, [event.target.name]: event.target.checked })
    }

    return <Box width={`100%`} flexDirection="column" display="flex" alignItems={"center"}>
        <Box width={`90%`} display={"flex"} flexDirection="row" justifyContent={"center"} alignItems={"center"}>
            <TextField
                className={classes.textField}
                label="Search the companies by name"
                variant="outlined"
                onChange={handleSearchPhraseChange} />
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Filter by specialty</FormLabel>
                <FormGroup row>
                    {Specialties.map((specialty, index) => <FormControlLabel
                        key={index}
                        control={<Checkbox key={index} checked={specialties[`${specialty}`]} onChange={handleSpecialtiesChange} name={specialty} />}
                        label={specialty}
                    />)}

                </FormGroup>
            </FormControl>
        </Box>
        <Box width={`90%`} padding={5} flexDirection="column" display="flex">
            <LinearProgress variant={isValidating ? `indeterminate` : `determinate`} value={100} color="primary" />
            <VirtualizerTable
                className={classes.table}
                data={Items || []}
                columns={columns}
            />
        </Box>
    </Box>

}