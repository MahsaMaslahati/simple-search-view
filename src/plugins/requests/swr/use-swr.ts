import useSWR from "swr"
import axios from "axios"

const setUseSwr = () => {
  return function useSwr<Data = unknown>(url: string) {
    const response = useSWR<Data, Error>(
      url || null,
      async () => {
        const { data } = await axios.get(url)
        return data
      },
      {
        revalidateOnFocus: false,
        shouldRetryOnError: false,
      },
    )

    if (response?.error) {
      console.error(response?.error)
    }

    return response
  }
}

export type useSwr<T> = { Items: T[] }
export const useSwr = setUseSwr()
