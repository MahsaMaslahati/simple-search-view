module.exports = {
  arrowParens: "always",
  trailingComma: "all",
  tabWidth: 2,
  semi: false,
  singleQuote: false,
  printWidth: 120,
  parser: "typescript",
  endOfLine: "auto",
}
